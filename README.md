# Money test homework

Here is a little testing project for Yandex money card issue page; underground night development used. Test cases are in /test/resources/money/test/cards.overview.feature file, written in Gherkin language.

This is my first experience of Kotlin, Page Object pattern and front end testing. Previously I've been implementing Cucumber back end tests with JS.

Currently the test has been run on Windows only; not guaranteed it will run smoothly on other systems without additional set up.

## Prerequisites

If you’d like to follow along, make sure you have the following installed:
- [JDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- An IDE; for instance [IntelliJ](https://www.jetbrains.com/idea/download/), which I use here.
- [Maven](https://maven.apache.org/)
- Mozilla Firefox browser
- geckodriver seen in path

## Run test

To run the test use maven

```bash
    mvn clean test
```
## Notes

- few scenarios are left from tutorials I've been reproducing