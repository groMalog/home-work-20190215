package money.pages

import money.pages.elements.CardsOverview
import money.test.support.World
import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.PageFactory
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory
import java.lang.Exception
import org.openqa.selenium.JavascriptExecutor

class CardsOverviewPage(driver: WebDriver) {

    private val mainContent: CardsOverview? = null

    init {
        PageFactory.initElements(HtmlElementDecorator(HtmlElementLocatorFactory(driver)), this)
    }

    fun clickButton(buttonName: String) {
        val button = when(buttonName.toLowerCase()) {
            "order plastic card" -> mainContent!!.plasticCardOrderButton
            "plastic card details" -> mainContent!!.plasticCardDetailsButton
            "virtual card details" -> mainContent!!.virtualCardDetailsButton
            "activate card" -> mainContent!!.cardActivateButton
            "ready card details" -> mainContent!!.readyCardDetailsButton
            "momentum card details" -> mainContent!!.momentumCardDetailsButton
            else -> throw Exception("Unknown button name: $buttonName")
        }!!
        (World.instance.driver!! as JavascriptExecutor).executeScript("arguments[0].scrollIntoView(true);", button)
        button.click()
    }
}