package money.pages.elements

import org.openqa.selenium.support.FindBy
import ru.yandex.qatools.htmlelements.annotations.Name
import ru.yandex.qatools.htmlelements.element.Button
import ru.yandex.qatools.htmlelements.element.HtmlElement

@Name("Cards overview")
@FindBy(className = "page-layout__main-content")
class CardsOverview : HtmlElement() {
    @Name("Order plastic card button")
    @FindBy(xpath = "//*[@id=\"yacard\"]/div/a[1]")
    val plasticCardOrderButton: Button? = null

    @Name("Plastic card details button")
    @FindBy(xpath = "//*[@id=\"yacard\"]/div/a[2]")
    val plasticCardDetailsButton: Button? = null

    @Name("Virtual card details button")
    @FindBy(xpath = "//*[@id=\"virtual\"]/div/a")
    val virtualCardDetailsButton: Button? = null

    @Name("Activate card button")
    @FindBy(xpath = "//*[@id=\"svyaznoy\"]/div/a[1]")
    val cardActivateButton: Button? = null

    @Name("Ready card details button")
    @FindBy(xpath = "//*[@id=\"svyaznoy\"]/div/a[2]")
    val readyCardDetailsButton: Button? = null

    @Name("Momentum card details button")
    @FindBy(xpath = "//*[@id=\"momentcard\"]/div/a")
    val momentumCardDetailsButton: Button? = null

}