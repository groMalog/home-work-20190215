package money.pages.elements

import org.openqa.selenium.support.FindBy
import ru.yandex.qatools.htmlelements.annotations.Name
import ru.yandex.qatools.htmlelements.element.HtmlElement
import ru.yandex.qatools.htmlelements.element.TextInput
import ru.yandex.qatools.htmlelements.element.Button


@Name("Search arrow")
@FindBy(xpath = "//form")
class SearchArrow : HtmlElement() {

    @Name("Search request input")
    @FindBy(id = "text")
    private val requestInput: TextInput? = null

    @Name("Search button")
    @FindBy(className = "suggest2-form__button")
    private val searchButton: Button? = null

    fun search(request: String) {
        requestInput!!.sendKeys(request)
        searchButton!!.click()
    }
}