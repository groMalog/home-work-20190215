package money.pages.elements

import org.openqa.selenium.support.FindBy
import ru.yandex.qatools.htmlelements.annotations.Name
import ru.yandex.qatools.htmlelements.element.HtmlElement

const val path = "/html/body/div[3]/div[1]/div[2]/div[1]/div[1]/ul"

@Name("Search results")
@FindBy(xpath = path)
class SearchResults : HtmlElement() {

    @Name("All result rows")
    @FindBy(className = "serp-item")
    private val allRows: List<HtmlElement>? = null

    fun row(index: Int): HtmlElement {
        return allRows!![index]
    }
}