package money.pages

import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator
import org.openqa.selenium.support.PageFactory
import org.openqa.selenium.WebDriver
import money.pages.elements.SearchArrow
import money.pages.elements.SearchResults


class SearchPage(driver: WebDriver) {

    private val searchArrow: SearchArrow? = null
    private val searchResults: SearchResults? = null

    init {
        PageFactory.initElements(HtmlElementDecorator(HtmlElementLocatorFactory(driver)), this)
    }

    fun search(request: String) {
        searchArrow!!.search(request)
    }

    fun resultRowText(index: Int): String {
        return searchResults!!.row(index).text
    }
}