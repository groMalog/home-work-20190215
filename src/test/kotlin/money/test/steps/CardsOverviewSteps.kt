package money.test.steps

import cucumber.api.java8.En
import money.pages.CardsOverviewPage
import money.test.support.World
import kotlin.test.assertTrue

class CardsOverviewSteps: En {

    private var cardsOverviewPage: CardsOverviewPage? = null

    private fun checkPage(pageName: String, bookmarkName: String = "") {
        val url = when(pageName.toLowerCase()) {
            "plastic card issue"       -> "https://money.yandex.ru/cards/issue"
            "plastic card details"     -> "https://money.yandex.ru/cards/yacard"
            "virtual card details"     -> "https://money.yandex.ru/cards/virtual"
            "momentum card activation" -> "https://money.yandex.ru/ymc/activate"
            "cards support"            -> "https://yandex.ru/support/money/cards/great-cards.html"
            else -> throw Exception("Unknown page name: $pageName")
        }
        assertTrue(World.instance.driver!!.currentUrl.startsWith(url))
        if (bookmarkName != "") {
            val bookmark = when(bookmarkName.toLowerCase()) {
                "pre-issued card" -> "#bc-pre-issued"
                else -> throw Exception("Unknown bookmark name: $bookmarkName")
            }
            assertTrue(World.instance.driver!!.currentUrl.contains(bookmark))
        }
    }

    init {
        Given("User opens Cards Overview page") {
            World.instance.driver!!.get("https://money.yandex.ru/cards")
            cardsOverviewPage = CardsOverviewPage(World.instance.driver!!)
        }

        When("User clicks {string} button") { buttonName: String ->
            cardsOverviewPage!!.clickButton(buttonName)
        }

        Then("{string} page is opened") { pageName: String ->
            checkPage(pageName)
        }

        Then("{string} page is opened at {string} bookmark") { pageName: String, bookmarkName: String ->
            checkPage(pageName, bookmarkName)
        }
    }
}