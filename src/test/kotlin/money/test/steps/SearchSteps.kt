package money.test.steps

import cucumber.api.java8.En
import money.pages.SearchPage
import money.test.support.World
import kotlin.test.assertTrue


class SearchSteps: En {

    private lateinit var searchPage: SearchPage

    init {

        Given("User opens Search page") {
            World.instance.driver!!.get("https://yandex.ru")
            searchPage = SearchPage(World.instance.driver!!)
        }

        When("User searches for {string}") { string: String ->
            searchPage.search(string)
        }

        Then("Search result page is opened") {
            assertTrue(World.instance.driver!!.currentUrl.startsWith("https://yandex.ru/search"))
        }

        Then("Search result page first result mentions {string}") { string: String ->
            assertTrue(searchPage.resultRowText(0).contains(string))
        }

    }
}