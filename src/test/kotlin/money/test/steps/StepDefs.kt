package money.test.steps

import cucumber.api.java8.En
import money.test.Belly
import kotlin.test.assertTrue

class StepDefs: En {
    lateinit var belly: Belly

    init {
        Given("^I have (\\d+) cukes in my belly$") { cukes: Int ->
            this.belly = Belly()
            belly.eat(cukes)
        }

        When("^I wait (\\d+) hour$") { arg1: Int ->
            Thread.sleep(1000)
        }

        Then("^my belly should growl$") {
            assertTrue(this.belly.checkGrowl())
        }
    }
}