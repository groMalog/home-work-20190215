package money.test.support

import cucumber.api.Scenario
import cucumber.api.java8.En
import org.openqa.selenium.WebDriver
import org.openqa.selenium.firefox.FirefoxDriver

class World: En {
    var driver: WebDriver? = null

    init {
        // cucumber creates only one instance of the class, so it will be saved to "instance" property
        instance = this

        Before {scenario: Scenario ->
            if (!scenario.sourceTagNames.contains("@tools:stand-alone")) {
                driver = FirefoxDriver()
            }
        }

        After {scenario: Scenario ->
            driver?.close()
        }
    }

    companion object {
        lateinit var instance: World
            private set
    }
}