Feature: Search page
  Of course Yandex has a search page
  (note: the purpose of the file is to check hoe Yandex htmlelements work with use of their sample)

  Scenario: Search for one word
    Given User opens Search page
    When User searches for "luck"
    Then Search result page is opened
    And Search result page first result mentions "luck"