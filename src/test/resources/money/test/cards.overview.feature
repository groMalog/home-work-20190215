Feature: Cards overview
  In order to have overview of all card offers
  As a user
  I want a page aggregating issue masters and documentation

  Background:
    Given User opens Cards Overview page

  Scenario: Proceed to plastic card issue master
    When User clicks "Order plastic card" button
    Then "Plastic Card Issue" page is opened

  Scenario: Proceed to plastic card details
    When User clicks "Plastic card details" button
    Then "Plastic Card Details" page is opened

  Scenario: Proceed to virtual card details
    When User clicks "Virtual card details" button
    Then "Virtual Card Details" page is opened

  Scenario: Proceed to momentum card activation master
    When User clicks "Activate card" button
    Then "Momentum Card Activation" page is opened

  Scenario: Proceed to details of momentum card taken from partner retailer
    When User clicks "Ready card details" button
    Then "Cards Support" page is opened

  Scenario: Proceed to details of momentum card taken from office
    When User clicks "Momentum card details" button
    Then "Cards Support" page is opened at "Pre-issued card" bookmark