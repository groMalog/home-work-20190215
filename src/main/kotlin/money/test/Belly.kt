package money.test


class Belly {
    private var cukesCount: Int = 0

    fun eat(cukes: Int) {
        this.cukesCount += cukes
    }

    fun checkGrowl(): Boolean {
        return this.cukesCount > 0
    }
}